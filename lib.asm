%define syscall_exit 60
%define syscall_write 1
%define std_out 1
%define std_err 2
%define space_symbol 0x20
%define newline_symbol 0xA
%define tab_symbol 0x9
%define min_number "0"
%define max_number "9"
%define minus_symbol "-"

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error

section .text


print_error:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, syscall_write
    mov rdi, std_err
    syscall
    ret

; Принимает код возврата и завершает текущий процесс
exit:
    xor rax, rax
	mov rax, syscall_exit
	syscall


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax, rax
.loop:
	cmp byte [rdi+rax], 0
	je .end
	inc rax
	jmp .loop
.end:
	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
; rdi - указатель на строку
print_string:
    call string_length
    mov rdx, rax
    mov rax, syscall_write
    mov rsi, rdi
    mov rdi, std_out
    syscall
    ret


; Принимает код символа и выводит его в stdout
print_char:
    mov rax, syscall_write
    push di
    mov rsi, rsp
    mov rdi, std_out
    mov rdx, 1
    syscall
    pop di
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, newline_symbol
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
        mov rax, rdi
        mov r8, rsp
        mov rdi, 0
        push rdi
.loop:
        xor rdx, rdx
        mov rdi, 10
        div rdi
        mov rdi, "0"
        add rdi, rdx
        dec rsp
        mov byte[rsp], dil
        xor rdi, rdi
        cmp rax, rdi
        jne .loop
        mov rdi, rsp
        call print_string
        mov rsp, r8
        ret


; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
        test rdi, rdi
        js .if_signed
        call print_uint
        ret
.if_signed:
        not rdi
        add rdi, 1
        push rdi
        mov rdi, "-"
        call print_char
        pop rdi
        call print_uint
        ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
        xor rax, rax
        xor rcx, rcx
.loop:
        mov al, byte[rdi+rcx]
        cmp al, byte[rsi+rcx]
        jne .end
        inc rcx
        cmp al, 0
        jne .loop
        xor rax, rax
        inc rax
        ret
.end:
        xor rax, rax
        ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
        push rdi
        push rsi
        push rdx
        xor rax, rax
        dec rsp
        xor rdi, rdi
        mov rdx, 1
        mov rsi, rsp
        syscall
        test rax, rax
        je .ret_end
        xor rax, rax
        mov al, byte [rsp]
        jmp .ret
.ret_end:
        xor rax, rax
.ret:
        inc rsp
        pop rdx
        pop rsi
        pop rdi
        ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
        xor r8, r8
        cmp r8, rsi
        je .ret_failed
.loop:
        call read_char
        cmp rax, space_symbol
        je .loop
        cmp rax, tab_symbol
        je .loop
        cmp rax, newline_symbol
        je .loop
.write:
        cmp r8, rsi
        je .ret_failed
        cmp rax, space_symbol
        je .ret_success
        cmp rax, tab_symbol
        je .ret_success
        cmp rax, newline_symbol
        je .ret_success
        test rax, rax
        je .ret_success
        mov byte[rdi + r8], al
        inc r8
        call read_char
        jmp .write
.ret_success:
        mov byte[rdi + r8], 0
        mov rax, rdi
        xor rdx, rdx
        mov rdx, r8
        ret
.ret_failed:
        xor rax, rax
        mov rdx, r8
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rdx, rdx
    xor r8, r8
    xor rax, rax
    xor rcx, rcx
    mov r9, 10
.loop:
    mov cl, [rdi+r8]
    cmp cl, min_number
    jb .ret
    cmp cl, max_number
    ja .ret
    sub cl, min_number
    mul r9
    add rax, rcx
    inc r8
    jmp .loop
.ret:
    mov rdx, r8
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rdx, rdx
    xor r8, r8
    xor rax, rax
    xor rcx, rcx
    xor rsi, rsi
    mov r9, 10
.loop:
    mov cl, [rdi+r8]
    cmp cl, minus_symbol
    je .add_minus
    cmp cl, min_number
    jb .ret
    cmp cl, max_number
    ja .ret
    sub cl, min_number
    mul r9
    add rax, rcx
    inc r8
    jmp .loop
.add_minus:
    test rsi, rsi
    jne .ret
    inc rsi
    inc r8
    jmp .loop
.ret:
    test rsi, rsi
    je .ret_without_minus
    not rax
    inc rax
    mov rdx, r8
    ret
.ret_without_minus:
    mov rdx, r8
    ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    call string_length
    cmp rax, rdx
    ja .ret
    xor rax, rax
.loop:
    mov cl, byte[rdi+rax]
    mov byte[rsi+rax], cl
    inc rax
    cmp cl, 0
    je .ret
    jmp .loop
.ret:
    xor rax, rax
    ret
