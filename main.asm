%include "lib.inc"
%include "colon.inc"
%include "words.inc"
extern find_word

%define MAX_BUFFER_SIZE 256
%define POINTER_SIZE 8

global _start

section .rodata
        input_message: db "Enter a key of the value:", 0
        overflow_message: db "Buffer overflow!", 0
        not_found_message: db "Value not found", 0
        colon_symbol: db " : ", 0

section .bss
        buffer: resb MAX_BUFFER_SIZE

section .text

_start:
        mov rdi, input_message
        call print_string
        call print_newline

        mov rdi, buffer
        mov rsi, MAX_BUFFER_SIZE
        call read_word

        test rax, rax
        jz .overflow

        ; сохраняем длину ключа
        push rdx

        mov rdi, rax
        mov rsi, NEXT
        call find_word
        test rax, rax
        jz .not_found

        ; печатаем ключ : значение
        push rax
        call print_newline
        pop rax
        add rax, POINTER_SIZE
        mov rdi, rax
        push rdi
        call print_string
        mov rdi, colon_symbol
        call print_string
        pop rdi

        ;возвращаем длину ключа
        pop rdx

        add rdi, rdx
        inc rdi  ; строку значения от строки ключа отделяет 0
        call print_string
        call print_newline
        call exit


.overflow:
        mov rdi, overflow_message
        jmp .exit
.not_found:
        mov rdi, not_found_message
.exit:
        call print_error
        call exit
