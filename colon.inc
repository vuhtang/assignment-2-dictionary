%define NEXT 0

%macro colon 2
        %ifid %2
                %2: dq NEXT
                %define NEXT %2
        %else
                %error "Wrong ID"
        %endif
        %ifstr %1
                db %1, 0
        %else
                %error "Key is not a string"
        %endif
%endmacro